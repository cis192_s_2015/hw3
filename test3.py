import unittest
import operator
from hw3 import *


class TestHomework3(unittest.TestCase):

    def setUp(self):
        pass

    # sort by id
    def test_sort_by_id(self):
        a = {'id': 10, 'name': 'Anne'}
        b = {'id': 5, 'name': 'Bob'}
        c = {'name': 'Charlie'}
        self.assertEqual(sort_by_id([a, b, c]), [c, b, a])

    # sort dict
    def test_sort_dict(self):
        d = {'a': 6, 'b': 2, 'c': 4}
        res = 'b=2, c=4, a=6'
        self.assertEqual(sort_dict(d), res)

    # count char dict
    def test_count_char_dict(self):
        c_dict = count_char_dict()
        self.assertEqual(c_dict['p']('apple'), 2)
        self.assertEqual(c_dict['a']('bananas'), 3)

    # collatz
    def test_collatz(self):
        self.assertEqual(collatz(6), ([6, 3, 10, 5, 16, 8, 4, 2, 1], 0))
        self.assertEqual(collatz(4), ([4, 2, 1], 4))
        self.assertEqual(collatz(13),
                         ([13, 40, 20, 10, 5, 16, 8, 4, 2, 1], 10))

    # my reduce
    def test_my_reduce(self):
        self.assertEqual(my_reduce(operator.mul, [1, 2, 3, 4, 5], 1), 120)
        list_of_list = [[1], [2], [3]]
        self.assertEqual(my_reduce(operator.add, list_of_list, []), [1, 2, 3])

    # partial app
    def test_my_partial(self):
        add_ten = my_partial(operator.add, 10)
        self.assertEqual(add_ten(5), 15)
        min_abs_or_2 = my_partial(min, 2, key=abs)
        self.assertEqual(min_abs_or_2(-1, 3), -1)
        self.assertEqual(min_abs_or_2(5, -3), 2)


def main():
    unittest.main()

if __name__ == '__main__':
    main()
